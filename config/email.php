<?php

$config['useragent'] = "php";
$config['protocol'] = "smtp";
$config['mailpath'] = "/usr/sbin/sendmail";
$config['smtp_timeout'] = 5;
$config['wordwrap'] = TRUE;
$config['wrapchars'] = 76;
$config['mailtype'] = "text";
$config['charset'] = "utf-8";
$config['validate'] = FALSE;
$config['priority'] = 3;
$config['crlf'] = "\r\n";
$config['newline'] = "\r\n" ;
$config['bcc_batch_mode'] = FALSE;
$config['bcc_batch_size'] = 50;
$config['mailtype'] = "html";

$config['smtp_host'] =  getenv("CI_CONFIG_EMAIL_SMTP_HOST");
$config['smtp_user'] = getenv("CI_CONFIG_EMAIL_SMTP_USER");
$config['smtp_pass'] = getenv("CI_CONFIG_EMAIL_SMTP_PASS");
$config['smtp_port'] = getenv("CI_CONFIG_EMAIL_SMTP_PORT");