<?php

class Persons extends CI_Controller {
    public function index() {
        $this->load->model("person");
        $this->load->view("layout", array(
            "view"=>"person_index",
            "persons"=> $this->person->all()
        ));
    }
    
    public function create() {
        if($this->input->post()) {
            $this->load->model("person");
            $id = $this->person->create($this->input->post("name"));
            $this->load->helper('url');
            redirect("/persons/view/$id");
        } else {
            $this->load->view("layout", array(
                "view"=>"person_create"
            ));
        }
    }
    
    public function view($id) {
        $this->load->model('person');
        $this->load->view("layout", array(
            "view"=>"person_view",
            "person" => $this->person->view($id)
        ));
    }
    
    public function edit($id) {
        if($this->input->post()) {
            $this->load->model("person");
            $this->person->edit($this->input->post("name"));
            $this->load->helper('url');
            redirect("/persons/view/$id");
        } else {
            $this->load->model('person');
            $this->load->view("layout", array(
                "view"=>"person_create",
                "person" => $this->person->view($id)
            ));
        }
    }
    
    public function remove($id) {
        $this->load->model('person');
        $this->person->remove($id);
        $this->load->helper('url');
        redirect("/");
    }
}