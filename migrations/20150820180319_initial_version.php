<?php

class Migration_Initial_version extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '9',
                'auto_increment' => true
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '32'
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('persons');
    }
    
    public function down() {
        $this->dbforge->drop_table('persons');
    }
}