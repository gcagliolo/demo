<a href="/persons/create">New person</a>
<table>
    <thead>
        <tr>
            <th>
                Name
            </th>
            <th colspan="2">
                Actions
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($persons as $person): ?>
            <tr>
                <td>
                    <?=html_escape($person->name)?>
                </td>
                <td>
                    <a href="/persons/view/<?=html_escape($person->id)?>">Detail</a>
                </td>
                <td>
                    <a href="/persons/remove/<?=html_escape($person->id)?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>